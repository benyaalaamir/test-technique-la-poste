# GitHub repo

With this project you can display the list of github projects of a user.
`GitHub repo` communicate with:

- github via rest api.

## Setup

First step to run the projet you need to install dependencies with the folling commande :

`npm install`

## Run

To lunch the project in developpement mode you need to run the following commande:
`npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## test

In this project, we have integrated two types of tests:

1. unit tests are lunched with the commande below:

    ```
    npm test
    ```

2. end 2 end tests are lunched with the commande below:

    ```
    npm run test:cypress 
    ```
  