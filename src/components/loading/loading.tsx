import React from 'react'



const Loading: React.FC = () => {
    return (
        <main className='loading-component'>
            Please wait while fetching data from server
        </main>
    )

}
export default Loading