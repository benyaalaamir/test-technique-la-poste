import React, { FormEvent, MutableRefObject, useEffect, useRef, useState } from "react";
import { dropdonwPropsType } from "../../utils/interfaces";
import { Icon } from "../Icon";


const Dropdown: React.FC<dropdonwPropsType> = (props) => {
    const [showMenu, setShowMenu] = useState<boolean>(false);
    const [selectedValue, setSelectedValue] = useState<string | null>(null);
    const [searchValue, setSearchValue] = useState<string>("");
    const searchRef = useRef() as MutableRefObject<HTMLInputElement>;
    const inputRef = useRef() as MutableRefObject<HTMLDivElement>;

    useEffect(() => {
        setSearchValue("");
        if (showMenu && searchRef.current) {
            searchRef.current.focus();
        }
    }, [showMenu]);

    useEffect(() => {
        const handler = ({ target }: MouseEvent): void => {
            if (inputRef.current && !inputRef.current.contains((target as Node))) {
                setShowMenu(false);
            }
        };
        window.addEventListener("click", handler);
        return () => {
            window.removeEventListener("click", handler);
        };
    }, []);
    const getDisplay = (): string => {
        if (!selectedValue) {
            return props.placeHolder;
        }
        return selectedValue;
    };

    const onItemClick = (option: string): void => {
        setSelectedValue(option);
        props.onChange(option);
    };
    const onSearch = (event: FormEvent<HTMLInputElement>) => {
        setSearchValue(event.currentTarget.value);
    };
    const isSelected = (option: string) => {
        if (!selectedValue) {
            return false;
        }
        return selectedValue === option;
    };
    const getOptions = (): string[] => {
        if (!searchValue) {
            return props.options
        }
        return props.options.filter((option: string) => option.toLowerCase().includes(searchValue.toLowerCase()))
    };
    const handleInputClick = (): void => {
        setShowMenu(!showMenu);
    };
    return (
        <div className="dropdown-container">
            <div ref={inputRef} className="dropdown-input" id="dropdown-input" onClick={handleInputClick}>
                <div className="dropdown-selected-value">{getDisplay()}</div>
                <div className="dropdown-tools">
                    <div className="dropdown-tool">
                        <Icon />
                    </div>
                </div>
            </div>
            {showMenu && (
                <div className="dropdown-menu">
                    <div className="search-box">
                        <input onChange={onSearch} id="search-box" value={searchValue} ref={searchRef} />
                    </div>
                    {getOptions().map((option: string, index: number) => (
                        <div
                            onClick={() => onItemClick(option)}
                            id={`option-${index}`}
                            key={option}
                            className={`dropdown-item ${isSelected(option) && "selected"}`}>
                            {option}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );


}
export default Dropdown;
