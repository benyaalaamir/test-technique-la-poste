import React from 'react'



const EmptyList: React.FC = () => {
    return (
        <main className='empty-list-component'>
            No repository found
        </main>
    )

}
export default EmptyList