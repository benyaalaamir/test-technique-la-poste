import React from 'react'
import { errorDisplayProps } from '../../utils/interfaces'



const ErrorDisplay: React.FC<errorDisplayProps> = ({
    message
}) => {
    return (
        <main className='error-display-component'>
            An error happend while fetching repositories detail: {message}
        </main>
    )

}
export default ErrorDisplay