import React from 'react'
import { useAppSelector } from '../../store'
import { selectAllRepositories } from '../../store/repositories'
import { Repository } from '../../utils/interfaces'
import { EmptyList } from '../EmptyList'
import { ErrorDisplay } from '../ErrorDisplay'
import { Loading } from '../Loading'
import { RepositoryCard } from '../RepositoryCard'



const ListRepositories: React.FC = () => {
    const repositories = useAppSelector(selectAllRepositories)
    const loading = useAppSelector((state) => state.repositories.loading)
    const error = useAppSelector((state) => state.repositories.error)
    if (loading) {
        return (
            <Loading />
        )
    }
    if (error) {
        const { message = '' } = error
        return <ErrorDisplay message={message} />

    }
    if (!repositories.length) {
        return <EmptyList />
    }
    return (
        <main className='flex-container'>
            <ul className="cards">
                {
                    repositories.map((repository: Repository) =>
                        <RepositoryCard key={repository.id} repository={repository} />
                    )
                }
            </ul>
        </main>
    )
}
export default ListRepositories