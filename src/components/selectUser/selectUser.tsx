import React, { useState } from 'react'
import { selectUserPropsType } from '../../utils/interfaces'
import { users } from '../../utils/users'
import { Dropdown } from '../Dropdown'




const SelectUser: React.FC<selectUserPropsType> = ({
    onSubmitForm
}) => {
    const [userInputValue, setUserInputValue] = useState<string>('')
    const [hasError, setHasError] = useState<boolean>(false)
    const validateForm = (event: React.SyntheticEvent) => {
        if (event && event.preventDefault) {
            event.preventDefault();
        }
        setHasError(false)
        if (!users.includes(userInputValue)) {
            setHasError(true)
            return;
        }
        onSubmitForm(userInputValue)
    }
    const handleOnChangeUserInputValue = (value: string): void => {
        setUserInputValue(value)
        setHasError(false)
    }
    return (
        <header className='select-user-component'>
            <form className="form-user" noValidate onSubmit={validateForm}>
                <div className='input-wrapper'>
                    <Dropdown options={users} placeHolder="Please select an user" onChange={handleOnChangeUserInputValue} />
                </div>
                {hasError && <div className='error-message'>
                    Please type an item from the following list: {users.join(', ')}
                </div>}
                <button className="seach-button" id="searchButton" >
                    Search
                </button>
            </form>
        </header>
    )
}
export default React.memo(SelectUser)