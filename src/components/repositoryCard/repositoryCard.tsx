import React from 'react'
import { repositoryProps } from '../../utils/interfaces'



const RepositoryCard: React.FC<repositoryProps> = ({
    repository
}) => {
    return (

        <li className="repository-item">
            <div className="card">
                <div className="card-header">
                    <div className="card-title">{repository.name}</div>   <div className="card-title full-name">{repository.full_name}</div>
                </div>

                <div className="card-content">
                    <div className="card-subcontent">
                        <div className='user-avatar' style={{
                            backgroundImage: `url(${repository.owner.avatar_url})`
                        }}></div>
                        <div className='card-watchers'>{repository.watchers}</div>
                    </div>
                    <div className='card-watchers'>{repository.language}</div>
                </div>
            </div>
        </li>



    )

}
export default RepositoryCard