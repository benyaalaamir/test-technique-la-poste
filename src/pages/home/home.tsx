import React, { useState } from 'react'
import { ListRepositories } from '../../components/ListRepositories'
import { SelectUser } from '../../components/SelectUser'
import { getRepositories } from '../../store/actions'
import { useAppDispatch } from '../../store'


const Home: React.FC = () => {
    const [onSubmitCalled, setOnSubmitCalled] = useState<boolean>(false)
    const dispatch = useAppDispatch()
    const onSubmit = (userName: string) => {
        dispatch(getRepositories(userName))
        setOnSubmitCalled(true)
    }
    return (
        <>
            <SelectUser onSubmitForm={onSubmit} />
            {onSubmitCalled ? <ListRepositories /> : null}
        </>
    )
}
export default Home