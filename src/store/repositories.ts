import {
  createSlice,
} from '@reduxjs/toolkit';
import { InitialState } from '../utils/interfaces';
import { getRepositories } from './actions';

const initialState: InitialState = {
  repositories: [],
  loading: false,
  error: null,
}

const repositoriesSlice = createSlice({
  name: 'repositories',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getRepositories.pending, (state) => {
      state.loading = true;
      state.error = null;
      state.repositories = []
    });
    builder.addCase(getRepositories.fulfilled, (state, action) => {
      state.loading = false;
      state.error = null;
      state.repositories = action.payload
    });
    builder.addCase(getRepositories.rejected, (state, action) => {
      state.error = action.error;
      state.loading = false;
      state.repositories = []
    });
  },
});

export default repositoriesSlice;

export const selectAllRepositories = (state: any) => state.repositories.repositories

