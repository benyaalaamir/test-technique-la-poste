import { createAsyncThunk } from "@reduxjs/toolkit";
import { baseURL } from "../utils/config";

export const getRepositories = createAsyncThunk('getRepositories', async (userName: string) => {
    const response = await fetch(`${baseURL}/users/${userName}/repos`);
    if (!response.ok) {
        throw await response.json()
    }
    return await response.json();
});