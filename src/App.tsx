import * as React from "react";
import { Routes, Route, Outlet } from "react-router-dom";
import { Home } from './pages/Home'
import { NotFound } from './pages/NotFound'
export default function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Outlet />}>
          <Route index element={<Home />} />
          <Route path="*" element={<NotFound />} />
        </Route>
      </Routes>
    </>
  );
}
