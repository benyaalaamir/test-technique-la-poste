import { configureStore } from '@reduxjs/toolkit';
import { getRepositories } from '../../store/actions';
import mockFetch from '../mocks/mockFetch';

describe('test getRepositories function', () => {
    beforeEach(() => {
        jest.spyOn(window, 'fetch').mockImplementation(mockFetch as jest.Mock);
    })

    afterEach(() => {
        jest.restoreAllMocks();
    });
    it('should get one element in the response', async () => {
        const store = configureStore({
            reducer: function (state, action) {
                switch (action.type) {
                    case 'getRepositories/fulfilled':
                        return action.payload;
                    default:
                        return state;
                }
            },
        });
        await store.dispatch(getRepositories("Amirbenyaala"));
        const state = store.getState();
        expect(state.length).toEqual(1);
        expect(state[0].name).toEqual('amir')
        expect(state[0].full_name).toEqual('amir ben yaala')
        expect(state[0].watchers).toEqual(1)

    });
});
