import { mockFetchResponse } from "../../utils/interfaces";
import { repository } from "./data";

export default async function mockFetch(url: RequestInfo | URL): Promise<mockFetchResponse> {

    return {
        ok: true,
        status: 200,
        json: async () => [
            repository
        ],
    };

}