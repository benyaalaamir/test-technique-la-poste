import { Repository } from "../../utils/interfaces";

export const repository: Repository = {
    id: "id",
    full_name: "amir ben yaala",
    name: "amir",
    language: "fullstackjs",
    watchers: 1,
    owner: {
        avatar_url: "https://avatars.githubusercontent.com/u/17980878?v=4"
    }

}