import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

import { NotFound } from '../../pages/NotFound';

describe('test NotFound page', () => {
    it('renders correctly', () => {
        const component = renderer.create(<BrowserRouter><NotFound /></BrowserRouter>).toJSON();
        expect(component).toMatchSnapshot();
    });
});