import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';

import { Home } from '../../pages/Home';
import store from '../../store';

describe('test Home page', () => {
    it('renders correctly', () => {
        const component = renderer.create(
            <Provider store={store}>
                <Home />
            </Provider>
        ).toJSON();
        expect(component).toMatchSnapshot();
    });
});