import React from 'react';
import renderer from 'react-test-renderer';

import { EmptyList } from '../../components/EmptyList';

describe('test EmptyList component', () => {
    it('renders correctly', () => {
        const component = renderer.create(<EmptyList />).toJSON();
        expect(component).toMatchSnapshot();
    });
})