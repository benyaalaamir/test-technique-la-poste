import React from 'react';
import renderer from 'react-test-renderer';

import { SelectUser } from '../../components/SelectUser';

describe('test SelectUser component', () => {
    it('renders correctly', () => {
        const component = renderer.create(<SelectUser onSubmitForm={(userName: string) => { }} />).toJSON();
        expect(component).toMatchSnapshot();
    });
});