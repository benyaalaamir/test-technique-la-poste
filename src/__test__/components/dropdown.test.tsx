import React from 'react';
import renderer from 'react-test-renderer';

import { Dropdown } from '../../components/Dropdown';
import { users } from '../../utils/users';

describe('test Dropdown component', () => {
    it('renders correctly', () => {
        const component = renderer.create(<Dropdown placeHolder='placeHolder' onChange={(value: string) => { }} options={users} />).toJSON();
        expect(component).toMatchSnapshot();
    });
})