import React from 'react';
import renderer from 'react-test-renderer';

import { Icon } from '../../components/Icon';

describe('test Icon component', () => {
    it('renders correctly', () => {
        const component = renderer.create(<Icon />).toJSON();
        expect(component).toMatchSnapshot();
    });
})