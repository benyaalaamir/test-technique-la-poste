import React from 'react';
import renderer from 'react-test-renderer';

import { Loading } from '../../components/Loading';

describe('test Loading component', () => {
    it('renders correctly', () => {
        const component = renderer.create(<Loading />).toJSON();
        expect(component).toMatchSnapshot();
    });
});