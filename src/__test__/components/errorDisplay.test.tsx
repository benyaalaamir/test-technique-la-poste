import React from 'react';
import renderer from 'react-test-renderer';

import { ErrorDisplay } from '../../components/ErrorDisplay';
describe('test ErrorDisplay component', () => {
    it('renders correctly when null message', () => {
        const component = renderer.create(<ErrorDisplay message={null} />)
        expect(component).toMatchSnapshot();
    });
    it('renders correctly when no message', () => {
        const component = renderer.create(<ErrorDisplay />).toJSON();
        expect(component).toMatchSnapshot();
    });
    it('renders correctly when message containe string', () => {
        const component = renderer.create(<ErrorDisplay message={"error"} />).toJSON();
        expect(component).toMatchSnapshot();
    });

    it('renders correctly with the correct message sent in props', () => {
        const message = "error"
        const component = renderer.create(<ErrorDisplay message={message} />);
        const instance = component.root.findByType("main").props.children;
        expect(instance.includes(message)).toBe(true);

    });
});