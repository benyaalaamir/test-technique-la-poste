import React from 'react';
import renderer from 'react-test-renderer';

import { RepositoryCard } from '../../components/RepositoryCard';
import { repository } from '../mocks/data';

describe('test RepositoryCard component', () => {
    it('renders correctly', () => {
        const component = renderer.create(<RepositoryCard repository={repository} />).toJSON();
        expect(component).toMatchSnapshot();
    });
});