export interface selectUserPropsType {
    onSubmitForm: (userName: string) => void
}
export interface dropdonwPropsType {
    onChange: (userName: string) => void,
    options: string[],
    placeHolder: string
}
export interface errorDisplayProps {
    message?: string | null
}

export interface repositoryProps {
    repository: Repository
}

export interface mockFetchResponse {
    ok: boolean,
    status: number,
    json: () => Promise<Repository[]>
}
type Owner = {
    avatar_url: string
}

export type Repository = {
    id: string;
    name: string;
    full_name: string;
    watchers: number;
    language: string;
    owner: Owner
}
export type InitialState = {
    repositories: Repository[],
    loading: boolean,
    error: any

}
