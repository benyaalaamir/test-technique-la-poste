describe('test home page ', () => {
  it('should render "Please type an item from the following list" ', () => {
    cy.visit('/')
    cy.get('button#searchButton').click()
    cy.contains('Please type an item from the following list')
  })
  it('should validate form without error ', () => {
    cy.visit('/')
    cy.get('#dropdown-input').click()
    cy.get('input').type("JakeWharton")
    cy.get('#dropdown-input').click()
    cy.contains('Please type an item from the following list').should('not.exist')
  })
  it('should render "An error happend while fetching repositories detail: Not Found', () => {
    cy.visit('/')
    cy.get('#dropdown-input').click()
    cy.get('input').type("Simulate an error case")
    cy.get('#option-0').click()
    cy.get('#searchButton').click()
    cy.contains('Please type an item from the following list').should('not.exist')
    cy.contains('An error happend while fetching repositories detail: Not Found')
  })
  it('should render "cards" ', () => {
    cy.visit('/')

    cy.get('#dropdown-input').click()
    cy.get('input').type("JakeWharton")
    cy.get('#option-0').click()
    cy.get('#searchButton').click()
    cy.contains('Please type an item from the following list').should('not.exist')
    cy.contains('An error happend while fetching repositories detail: Not Found').should('not.exist')

  })
  it('should render "No repository found" ', () => {
    cy.visit('/')
    cy.intercept(
      {
        method: 'GET',
        url: '/users/JakeWharton/repos',
      },
      []
    )
    cy.get('#dropdown-input').click()
    cy.get('input').type("JakeWharton")
    cy.get('#option-0').click()
    cy.get('#searchButton').click()
    cy.contains('Please type an item from the following list').should('not.exist')
    cy.contains('An error happend while fetching repositories detail: Not Found').should('not.exist')
    cy.contains('No repository found')

  })

})

